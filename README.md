# Netaura Experience

Repositório destinado ao ambiente 3D do jogo AmongNet, que executa em plataformas *web*.
Disponível em: https://enigmatic-ocean-95464.herokuapp.com/

![imagem de exemplo](https://gitlab.com/netaura/netauraexperience/-/raw/master/public/assets/experience.png)

## Instalação 
```
npm install 
```
## Execução
```
npm start 

```
## Abrir
```
http://localhost:8080
```

Construído com [Networked-A-Frame](https://github.com/haydenjameslee/networked-aframe), uma estrutura da Web para criar experiências de realidade virtual multiusuário. Funciona em plataformas Vive, Rift, desktop e móveis.

## Planilhas Auxiliares

As planilhas se encontram dentro da pasta "planilhas" dentro desse repositório.

As planilhas das equipes são criadas através de um *script*.

Deve ser realizado o *upload* dessa pasta dentro do Google Drive, e ela seguirá a seguinte estrutura: 

```
  .
  ├── Equipes                         # Pasta contendo arquivos referentes as equipes
  │   ├── Equipes                     # Planilha com as composições das equipes
  │   ├── Progresso Equipes           # Progresso das equipes
  ├── TabelasEquipes                  # Pasta contendo as planilhas das equipes
  │   ├── Equipe 1                    # Planilha da Equipe 1
  │   ├── Equipe 2                    # Planilha da Equipe 2
  │   ├── Equipe 3                    # Planilha da Equipe 3
  │   ...                     
  │   └── Equipe 10                   # Planilha da Equipe 10
  ├── Admin                           # Planilha de administração da experiência 
  ├── Netaura Equipes Links.json      # Arquivo com IDs das planilhas das equipes
  └── Template Equipe                 # Template referente as planilhas das equipes
```

## Configurações necessárias

  1. Inicialmente deve-se "Salvar como Planilhas Google" cada arquivo ".xlsx" e deletar os arquivos ".xlsx".

  2. Na planinha "Progresso Equipes" é necessário importar a página de síntese de dados da planinha de "Admin" na celula "F2" e permitir acesso:

  ```
  IMPORTRANGE("https://docs.google.com/spreadsheets/d/**ID_PLANILHA_ADMIN**/edit";"SintesedeDados!A1:B10")
  ```

  3. Na planinha "Admin" é necessário importar a planilha "Equipes" na celula "A2" e permitir acesso:

  ```
  IMPORTRANGE("https://docs.google.com/spreadsheets/d/**ID_PLANILHA_EQUIPES**/edit";"Equipes!A1:E20")
  ```

  4. Na planilha de "Admin" é necessário colar o *script* abaixo usando a opção Extensões -> Apps Script e salvar. Além disso, também é preciso adicionar o serviço Drive em "Serviços".

  ```
  function iniciarDinamica() {
    var equipeFiles = DriveApp.getFolderById("**ID_FOLDER_DAS_PLANILHAS_DAS_EQUIPES**").getFiles();
    var equipes = SpreadsheetApp.openById("**ID_DA_PLANILHA_DE_COMPOSIÇÃO_DAS_EQUIPES**");

    while (equipeFiles.hasNext()){
      let file = equipeFiles.next();

      let equipeNumber = file.getName().match(/\d+/)[0] - 1;

      let sheet = equipes.getSheets()[0];
      let data = sheet.getDataRange().getValues();

      let equipe = [
        data[ 1 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ], // [ 1 ou 11 ][ 0, 1, 2, 3, 4 ]
        data[ 2 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 3 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 4 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 5 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 9 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 7 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 8 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
        data[ 9 + 10 * ( equipeNumber >= 5 ) ][ equipeNumber % 5 ],
      ]

      equipe = equipe.filter(Boolean);
    
      if (equipe.length > 0 ) {
        let fileID = file.getId();

        for ( var i = 0; i < equipe.length; i++ ) {
            Drive.Permissions.insert(
            {
              'role': 'writer',
              'type': 'user',
              'value': equipe[i]
            },
              fileID,
            {
              'sendNotificationEmails': 'false'
            });
        };
      }
    }
  }

  function createEquipeFiles() {
    var file = DriveApp.getFileById(**ID do arquivo "Template Equipe"**);
    var folder = DriveApp.getFolderById(**ID da pasta "TabelasEquipes"**);
    var data = DriveApp.getFileById("ID do arquivo "Netaura Equipes Links.json"");

    setTrashedFilesInsideFolder(folder);

    var files = [];

    for ( let i=1; i<11; i++){
      var newFile = file.makeCopy(`Equipe ${ i }`, folder);
      var sheetFile = SpreadsheetApp.open(newFile);
      var sheet = sheetFile.getSheets()[0];
      sheet.getRange('a1:f1').setValue(`Equipe ${ i }`);
      files.push({ id: newFile.getId(), index: i, name: `Equipe ${ i }` })
    }

    data.setContent( JSON.stringify(files) );
  }

  function setTrashedFilesInsideFolder(folder) {
    var files = folder.getFiles();
    while (files.hasNext()){
      let file = files.next();
      file.setTrashed(true)
    }
  }

  ```

  Após colocar e salvar esse script, é necessário vincular as funções as imagens "Iniciar Dinâmica" e "Criar Arquivos das Equipes", para isso basta clicar com o botão direito do mouse sobre as imagens, clicar nos 3 pontinhos e em "Transferir Script" colocando a função correspondente, sendo elas: "iniciarDinamica" ou "createEquipeFiles".

  5. Com as configurações 1, 2 e 3 realizadas corretamentes, para criar as planilhas das equipes basta clicar em "Criar Arquivos das Equipes" na planilha "Admin". Uma vez criadas as planilhas é necessario importa-las na página "Progresso" da planilha "Admin", no lugar dos "#REF!".

  6. Dessa forma, com tu tudo configurado, para inicar a experiência basta clicar em "Iniciar Dinâmica".